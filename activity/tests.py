from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("activity:activity")
        self.test_activity = Activity.objects.create(
            activity = "joget"
        )
        self.test_member = Member.objects.create(
            member = "fala",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("activity:delete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("activity:delete_member",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "activity.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "joget"
        }, follow=True)
        self.assertContains(response, "joget")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "dewi",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "dewi")

