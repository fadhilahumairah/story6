from django.shortcuts import render,redirect
from .models import Member, Activity
from .forms import MemberForm ,ActivityForm
from django.contrib import messages
# Create your views here.

def activity(request):
    if request.method == "POST":
        formA = ActivityForm(request.POST)
        formB = MemberForm(request.POST)
        if formA.is_valid():
            formA.save()
            return redirect('activity:activity')
        elif formB.is_valid():
            formB.save()
            return redirect('activity:activity')
    else:
        formA = ActivityForm()
        formB = MemberForm()
        activities = Activity.objects.all()
        members = Member.objects.all()
        context = {
            'formA' : formA,
            'formB' : formB,
            'activities' : activities,
            'members' : members
        }
        return render(request, 'activity.html', context)

def delete(request,id):
    Activity.objects.get(id=id).delete()
    return redirect('activity:activity')

def delete_member(request,id):
    Member.objects.get(id=id).delete()
    return redirect('activity:activity')

