from django.urls import path

from . import views

app_name = 'activity'

urlpatterns = [
    path('', views.activity, name='activity'),
    path('delete/<str:id>', views.delete, name='delete'),
    path('delete/member/<str:id>', views.delete_member, name='delete_member')
]
